﻿using System;
using AAIFinalAssignment.Compute;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AAIFinalAssignmentTests
{
    [TestClass, TestCategory("Vector")]
    public class VectorTests
    {
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Vector_WhenDataShorterThan2_Crash()
        {
            new Vector(0d);
        }

        [TestMethod]
        public void Vector_WhenDataNotShorterThan2_DoNotCrash()
        {
            new Vector(0d, 0d);
            new Vector(0d, 0d, 0d);
            new Vector(0d, 0d, 0d, 0d);
            new Vector(0d, 0d, 0d, 0d, 0d);
        }

        [TestMethod]
        public void Vector_WhenSizeIsN_SizeReturnsN()
        {
            Vector v;

            v = new Vector(0d, 0d);
            Assert.AreEqual(2, v.Size);

            v = new Vector(0d, 0d, 0d);
            Assert.AreEqual(3, v.Size);
        }

        [TestMethod]
        public void Vector_Length()
        {
            Vector v;

            v = new Vector(5d, 5d);
            Assert.AreEqual(7.07106d, v.Length(), 0.00001d);

            v = new Vector(1d, 3d);
            Assert.AreEqual(3.16227d, v.Length(), 0.00001d);
        }

        [TestMethod]
        public void Vector_Normalize()
        {
            Vector v;

            v = new Vector(5d, 0d);
            v = v.Normalize();
            Assert.AreEqual(1, v.X);
            Assert.AreEqual(0, v.Y);

            v = new Vector(3d, 6d);
            v = v.Normalize();
            Assert.AreEqual(0.44721d, v.X, 0.00001d);
            Assert.AreEqual(0.89442d, v.Y, 0.00001d);
        }

        [TestMethod]
        public void Vector_Add()
        {
            Vector v;

            v = new Vector(1d, 2d) + new Vector(2d, 1d);
            Assert.AreEqual(2, v.Size);
            Assert.AreEqual(3, v.X, 0.00001d);
            Assert.AreEqual(3, v.Y, 0.00001d);

            v = new Vector(26.3d, 14.0d, 19.9d) + new Vector(3d, 4.2d, 6d);
            Assert.AreEqual(3, v.Size);
            Assert.AreEqual(26.3d + 3d, v.X);
            Assert.AreEqual(14.0d + 4.2d, v.Y);
            Assert.AreEqual(19.9d + 6d, v.Z);
        }
    }
}
