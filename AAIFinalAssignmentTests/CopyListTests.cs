﻿using AAIFinalAssignment.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AAIFinalAssignmentTests
{
    [TestClass, TestCategory("CopyList")]
    public class CopyListTests
    {

        [TestMethod]
        public void CopyList_Add()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.Aggregate(0, (acc, cur) =>
            {
                Assert.IsTrue(cur > acc);
                return cur;
            });
            Assert.AreEqual(3, list.Count);
        }

        [TestMethod]
        public void CopyList_Remove_Front()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.Remove(1);
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(2, list[0]);
            Assert.AreEqual(3, list[1]);
        }

        [TestMethod]
        public void CopyList_Remove_Middle()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.Remove(2);
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(3, list[1]);
        }

        [TestMethod]
        public void CopyList_Remove_Back()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.Remove(3);
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
        }

        [TestMethod]
        public void CopyList_Clear()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.Clear();
            Assert.AreEqual(0, list.Count);
        }

        [TestMethod]
        public void CopyList_Contains_WhenContained_ReturnsTrue()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            Assert.IsTrue(list.Contains(2));
        }

        [TestMethod]
        public void CopyList_Contains_WhenNotContained_ReturnsFalse()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            Assert.IsFalse(list.Contains(4));
        }

        [TestMethod]
        public void CopyList_Count()
        {
            CopyList<int> list = new CopyList<int>();
            Assert.AreEqual(0, list.Count);
            list.Add(1);
            Assert.AreEqual(1, list.Count);
            list.Add(2);
            Assert.AreEqual(2, list.Count);
            list.Add(3);
            Assert.AreEqual(3, list.Count);
        }

        [TestMethod]
        public void CopyList_CopyTo()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            int[] arr = new int[3];
            list.CopyTo(arr, 0);

            Assert.AreEqual(1, arr[0]);
            Assert.AreEqual(2, arr[1]);
            Assert.AreEqual(3, arr[2]);
        }

        [TestMethod]
        public void CopyList_IndexOf_WhenContained_ReturnsIndex()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            Assert.AreEqual(0, list.IndexOf(1));
            Assert.AreEqual(1, list.IndexOf(2));
            Assert.AreEqual(2, list.IndexOf(3));
        }

        [TestMethod]
        public void CopyList_IndexOf_WhenNotContained_ReturnsNegativeOne()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            Assert.AreEqual(-1, list.IndexOf(0));
            Assert.AreEqual(-1, list.IndexOf(4));
            Assert.AreEqual(-1, list.IndexOf(-5));
        }

        [TestMethod]
        public void CopyList_Insert_WhenInFront_ShouldRemove()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(2);
            list.Add(3);

            list.Insert(0, 1);

            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
        }

        [TestMethod]
        public void CopyList_Insert_WhenInCenter_ShouldRemove()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(3);

            list.Insert(1, 2);

            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
        }

        [TestMethod]
        public void CopyList_Insert_WhenInBack_ShouldRemove()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);

            list.Insert(2, 3);

            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
        }

        [TestMethod]
        public void CopyList_RemoveAt_WhenInFront_ShouldRemove()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.RemoveAt(0);
            
            Assert.AreEqual(2, list[0]);
            Assert.AreEqual(3, list[1]);
        }

        [TestMethod]
        public void CopyList_RemoveAt_WhenInCenter_ShouldRemove()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.RemoveAt(1);

            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(3, list[1]);
        }

        [TestMethod]
        public void CopyList_RemoveAt_WhenInBack_ShouldRemove()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.RemoveAt(2);

            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
        }

        [TestMethod, ExpectedException(typeof(IndexOutOfRangeException))]
        public void CopyList_RemoveAt_WhenOutOfBounds_ShouldThrowException()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.RemoveAt(3);
        }

        [TestMethod]
        public void CopyList_Remove_WhenContained_ShouldRemove()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            Assert.IsTrue(list.Remove(2));

            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(3, list[1]);
        }

        [TestMethod]
        public void CopyList_Remove_WhenNotContained_ShouldNotRemove()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            Assert.IsFalse(list.Remove(4));

            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
        }

        [TestMethod]
        public void CopyList_RemoveAll_WhenPredReturnsTrue_ShouldRemoveElement()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            list.RemoveAll(i => i == 2);

            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(3, list[1]);
        }

        [TestMethod]
        public void CopyList_GetEnumerator_WhenChangedDuringEnumeration_ShouldNotCrash()
        {
            CopyList<int> list = new CopyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            IEnumerator<int> enumerator = list.GetEnumerator();
            enumerator.MoveNext();

            Assert.AreEqual(1, enumerator.Current);
            Assert.IsTrue(enumerator.MoveNext());

            list.Insert(1, 5);
            Assert.AreEqual(4, list.Count);

            Assert.AreEqual(2, enumerator.Current);
            Assert.IsTrue(enumerator.MoveNext());
            Assert.AreEqual(3, enumerator.Current);
            Assert.IsFalse(enumerator.MoveNext());
        }

    }
}
