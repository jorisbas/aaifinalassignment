﻿using AAIFinalAssignment.Compute;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AAIFinalAssignmentTests
{
    [TestClass, TestCategory("Rect")]
    public class RectTests
    {

        [TestMethod]
        public void Rect_ConstructorWidthHeight_WhenEnteredCorrectly()
        {
            BoundingBox r;

            r = new BoundingBox(10, 10);
            Assert.AreEqual(0, r.Left);
            Assert.AreEqual(0, r.Top);
            Assert.AreEqual(9, r.Right);
            Assert.AreEqual(9, r.Bottom);
            Assert.AreEqual(10, r.Width);
            Assert.AreEqual(10, r.Height);

            r = new BoundingBox(13.3d, 3.0d);
            Assert.AreEqual(0, r.Left);
            Assert.AreEqual(0, r.Top);
            Assert.AreEqual(12.3d, r.Right);
            Assert.AreEqual(2d, r.Bottom);
            Assert.AreEqual(13.3d, r.Width);
            Assert.AreEqual(3.0d, r.Height);
        }

        [TestMethod]
        public void Rect_ConstructorCoordinates_WhenEnteredCorrectly()
        {
            BoundingBox r = new BoundingBox(0d, 1d, 4d, 5d);
            Assert.AreEqual(0d, r.Left);
            Assert.AreEqual(1d, r.Top);
            Assert.AreEqual(4d, r.Right);
            Assert.AreEqual(5d, r.Bottom);
            Assert.AreEqual(5d, r.Width);
            Assert.AreEqual(5d, r.Height);
        }

        [TestMethod]
        public void Rect_ConstructorCoordinates_WhenEnteredIncorrect_ThenCorrectCoordinates()
        {
            BoundingBox r = new BoundingBox(4d, 5d, 0d, 1d);
            Assert.AreEqual(0d, r.Left);
            Assert.AreEqual(1d, r.Top);
            Assert.AreEqual(4d, r.Right);
            Assert.AreEqual(5d, r.Bottom);
            Assert.AreEqual(5d, r.Width);
            Assert.AreEqual(5d, r.Height);
        }

        [TestMethod]
        public void Rect_TopLeftQuadrant()
        {
            BoundingBox r = new BoundingBox(10, 10);
            BoundingBox q = r.TopLeftQuadrant;
            Assert.AreEqual(0, q.Left);
            Assert.AreEqual(0, q.Top);
            Assert.AreEqual(4, q.Right);
            Assert.AreEqual(4, q.Bottom);
            Assert.AreEqual(5, q.Width);
            Assert.AreEqual(5, q.Height);
        }

        [TestMethod]
        public void Rect_TopRightQuadrant()
        {
            BoundingBox r = new BoundingBox(10, 10);
            BoundingBox q = r.TopRightQuadrant;
            Assert.AreEqual(5, q.Left);
            Assert.AreEqual(0, q.Top);
            Assert.AreEqual(9, q.Right);
            Assert.AreEqual(4, q.Bottom);
            Assert.AreEqual(5, q.Width);
            Assert.AreEqual(5, q.Height);
        }

        [TestMethod]
        public void Rect_BottomLeftQuadrant()
        {
            BoundingBox r = new BoundingBox(10, 10);
            BoundingBox q = r.BottomLeftQuadrant;
            Assert.AreEqual(0, q.Left);
            Assert.AreEqual(5, q.Top);
            Assert.AreEqual(4, q.Right);
            Assert.AreEqual(9, q.Bottom);
            Assert.AreEqual(5, q.Width);
            Assert.AreEqual(5, q.Height);

            r = new BoundingBox(800, 600);
            q = r.BottomLeftQuadrant;
            Assert.AreEqual(0, q.Left);
            Assert.AreEqual(300, q.Top);
            Assert.AreEqual(800 / 2 - 1, q.Right);
            Assert.AreEqual(599, q.Bottom);
            Assert.AreEqual(400, q.Width);
            Assert.AreEqual(300, q.Height);
        }

        [TestMethod]
        public void Rect_BottomRightQuadrant()
        {
            BoundingBox r = new BoundingBox(10, 10);
            BoundingBox q = r.BottomRightQuadrant;
            Assert.AreEqual(5, q.Left);
            Assert.AreEqual(5, q.Top);
            Assert.AreEqual(9, q.Right);
            Assert.AreEqual(9, q.Bottom);
            Assert.AreEqual(5, q.Width);
            Assert.AreEqual(5, q.Height);
        }
    }
}
