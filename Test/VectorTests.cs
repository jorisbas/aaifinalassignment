﻿using AAIFinalAssignment.Compute;
using Xunit;

namespace Test
{
	public class VectorTests
	{
		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(10)]
		[InlineData(100)]
		[InlineData(1000)]
		public void VectorShouldCreateFromLength(int len)
		{
			Vector v = new Vector(len);
			
			Assert.Equal(len, v.Size);
			Assert.Equal(0d, v[0]);
		}
	}
}