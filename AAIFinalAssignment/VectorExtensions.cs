﻿using System.Reflection;
using System.Windows.Forms;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment
{
	public static class VectorExtensions
	{
		public static Vector Truncate(this Vector vector, double max)
		{
			double i = max / vector.Length();
			i = i < 1.0 ? i : 1;
			return vector * i;
		}

		public static void EnableDoubleBuffer(Control c)
		{
			PropertyInfo aProp =
				typeof(Control).GetProperty(
					"DoubleBuffered", BindingFlags.NonPublic | BindingFlags.Instance);
			aProp.SetValue(c, true, null);
		}
	}
}