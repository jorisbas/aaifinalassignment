﻿using System;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment
{
	public class Object2DFactory
	{
		public Object2D CreateTriangle(double size)
		{
			size /= 2;
			Object2D o2D = new Object2D();
			o2D.AddPoint(0, size);
			o2D.AddPoint(size - size / 2, -size + size / 2);
			o2D.AddPoint(-size + size / 2, -size + size / 2);
			o2D.Edges.Add(new Tuple<int, int>(0, 1));
			o2D.Edges.Add(new Tuple<int, int>(1, 2));
			o2D.Edges.Add(new Tuple<int, int>(2, 0));
			return o2D;
		}

		public Obstacle CreateObstacle(double x, double y, double size)
		{
			Obstacle obstacle = new Obstacle();
			obstacle.Position = new Vector(new[] {x, y});
			obstacle.Size = size;
			return obstacle;
		}
	}
}