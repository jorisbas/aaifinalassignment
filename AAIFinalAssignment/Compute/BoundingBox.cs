﻿using System;

namespace AAIFinalAssignment.Compute
{
    public class BoundingBox : BoundingBox.IIntersector, BoundingBox.IContainable, Vector.IIntersector
    {

        public Vector TopLeft { get; }
        public Vector TopRight
        {
            get
            {
                return new Vector(Right, Top);
            }
        }
        public Vector BottomLeft
        {
            get
            {
                return new Vector(Left, Bottom);
            }
        }
        public Vector BottomRight { get; }

        public double Left { get { return TopLeft.X; } }
        public double Top { get { return TopLeft.Y; } }
        public double Right { get { return BottomRight.X; } }
        public double Bottom { get { return BottomRight.Y; } }

        public double DeltaX { get { return Right - Left; } }
        public double DeltaY { get { return Bottom - Top; } }

        public double Width { get { return DeltaX + 1; } }
        public double Height { get { return DeltaY + 1; } }

        public Vector Delta { get { return BottomRight - TopLeft; } }

        #region Quadrants

        public BoundingBox TopLeftQuadrant
        {
            get
            {
                return new BoundingBox(
                    TopLeft,
                    new Vector(
                        Left + (Width / 2) - 1,
                        Top + (Height / 2) - 1));
            }
        }
        public BoundingBox TopRightQuadrant
        {
            get
            {
                return new BoundingBox(
                    new Vector(Left + (Width / 2), Top),
                    new Vector(Right, Top + (Height / 2) - 1));
            }
        }
        public BoundingBox BottomLeftQuadrant
        {
            get
            {
                return new BoundingBox(
                    new Vector(Left, Top + (Height / 2)),
                    new Vector(Left + (Width / 2) - 1, Bottom));
            }
        }
        public BoundingBox BottomRightQuadrant
        {
            get
            {
                return new BoundingBox(
                    new Vector(Left + (Width / 2), Top + (Height/ 2)),
                    BottomRight);
            }
        }

        #endregion

        public BoundingBox(double width, double height)
            : this(0, 0, width - 1, height - 1)
        {
        }

        public BoundingBox(double x1, double y1, double x2, double y2)
            : this(_TopLeft(x1, y1, x2, y2),
                   _BottomRight(x1, y1, x2, y2))
        {
        }

        public BoundingBox(Vector topLeft, Vector bottomRight)
        {
            if (topLeft.X > bottomRight.X || topLeft.Y > bottomRight.Y)
            {
                // If topLeft is not above and left of bottomRight, correct it.
                TopLeft = _TopLeft(topLeft.X, topLeft.Y, bottomRight.X, bottomRight.Y);
                BottomRight = _BottomRight(topLeft.X, topLeft.Y, bottomRight.X, bottomRight.Y);
            }
            else
            {
                // Use given Vectors otherwise.
                TopLeft = topLeft;
                BottomRight = bottomRight;
            }
        }

        public bool Intersects(BoundingBox rect)
        {
            return Left < rect.Right && Right > rect.Left &&
                Top < rect.Bottom && Bottom > rect.Top;
        }

        public bool Intersects(Vector vec)
        {
            return Left <= vec.X && Right > vec.X &&
                Top <= vec.Y && Bottom > vec.Y; 
        }

        public bool ContainedIn(BoundingBox rect)
        {
            return Left >= rect.Left && Right <= rect.Right &&
                Top >= rect.Top && Bottom <= rect.Bottom;
        }

        private static Vector _TopLeft(double x1, double y1, double x2, double y2)
        {
            return new Vector(Math.Min(x1, x2), Math.Min(y1, y2));
        }
        private static Vector _BottomRight(double x1, double y1, double x2, double y2)
        {
            return new Vector(Math.Max(x1, x2), Math.Max(y1, y2));
        }

        public interface IIntersector
        {
            bool Intersects(BoundingBox rect);
        }

        public interface IContainable
        {
            bool ContainedIn(BoundingBox rect);
        }

    }
}
