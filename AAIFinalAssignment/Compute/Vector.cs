﻿using AAIFinalAssignment.Extensions;
using System;
using System.Drawing;
using System.Linq;

namespace AAIFinalAssignment.Compute
{
	public class Vector : BoundingBox.IContainable
	{
		private readonly double[] _data;

		public int Size => _data.Length;

		public int Width => 1;

		public int Height => Size;

		#region Helper properties

		public double X => this[0];

		public double Y => this[1];

		public double Z => this[2];

		#endregion

		public Vector(params double[] data)
		{
//            if (data.Length < 2)
//                throw new Exception("Vector must have at least 2 components");
			_data = data;
		}

		public Vector(int len)
		{
			_data = new double[len];
		}

		public double Length()
		{
			double accum = 0;
			for (int i = 0; i < Size; i++)
				accum += Math.Pow(this[i], 2);
			return Math.Sqrt(accum);
		}

		public Vector Normalize()
		{
            double len = Length();
			return (len > 0) ? this / len : this;
		}

		public double Dot(Vector b)
		{
			if (Size != b.Size)
				throw new Exception("Vector sizes do not match");

			return _data
				.Take(Size - 1) // Ignore 'w' field
				.Select((va, i) => va * b[i])
				.Sum();
		}

        public Vector Resize(int size)
        {
            Vector v = new Vector(size);
            for (int i = 0; i < size; i++)
                v[i] = (Size > i) ? this[i] : 1;
            return v;
        }

		public Matrix ToMatrix()
		{
			double[,] m = new double[Size, 1];
			for (int i = 0; i < Size; i++)
				m[i, 0] = _data[i];
			return new Matrix(m);
		}

		public double this[int i]
		{
			get => _data[i];
			set => _data[i] = value;
		}

		public override string ToString()
		{
			return $"Vector({_data.Select(d => string.Format("{0:00}", d)).Join()})";
		}

		#region Operators

		private static Vector _merge(Vector a, Vector b, Func<double, double, double> merger, bool scalar = false)
		{
			if (a.Size != b.Size)
				throw new Exception("Vector sizes do not match");
			return new Vector(a._data
				.Take(a.Size - (scalar ? 1 : 0)) // Scalar? Truncate the last field.
				.Select((va, i) => merger(va, b[i]))
				.Concat(scalar ? new[] {a[a.Size - 1]} : new double[] { }) // Scalar? Re-add the last field.
				.ToArray());
		}

		public bool ContainedIn(BoundingBox rect)
		{
			// TODO: Unit testing
			return X >= rect.Left && Y >= rect.Top &&
			       X <= rect.Right && Y <= rect.Bottom;
		}

		public static Vector operator +(Vector a, Vector b)
		{
			return _merge(a, b, (va, vb) => va + vb);
		}

		public static Vector operator -(Vector a, Vector b)
		{
			return _merge(a, b, (va, vb) => va - vb);
		}

		public static Vector operator -(Vector a)
		{
			return new Vector(a._data.Select(v => -v).ToArray());
		}

		public static Vector operator *(Vector a, Vector b)
		{
			return _merge(a, b, (va, vb) => va * vb);
		}

		public static Vector operator *(Vector a, double b)
		{
			return new Vector(a._data.Select(va => va * b).ToArray<double>());
		}

		public static Vector operator *(double a, Vector b)
		{
			return b * a;
		}

		public static Vector operator /(Vector a, Vector b)
		{
			return _merge(a, b, (va, vb) => va / vb);
		}

		public static Vector operator /(Vector a, double b)
		{
			return new Vector(a._data.Select(va => va / b).ToArray<double>());
		}

		public static Vector operator /(double a, Vector b)
		{
			return new Vector(b._data.Select(vb => a / vb).ToArray<double>());
		}

		public static explicit operator Matrix(Vector a)
		{
			return a.ToMatrix();
		}

        public static explicit operator PointF(Vector a)
        {
            return new PointF((float)a[0], (float)a[1]);
        }

		#endregion


		public interface IIntersector
		{
			bool Intersects(Vector vec);
		}
	}
}