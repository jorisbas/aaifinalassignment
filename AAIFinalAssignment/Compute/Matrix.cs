﻿using System;
using System.Text;

namespace AAIFinalAssignment.Compute
{
	public class Matrix
	{
		private readonly double[,] _data;

		public int Height
		{
			get { return _data.GetLength(0); }
		}

		public int Width
		{
			get { return _data.GetLength(1); }
		}

		public Matrix(double[,] data)
		{
			_data = data;
		}

		public Matrix Truncate(int w, int h)
		{
			if (w > Width || h > Height)
				throw new Exception("Matrix truncate cannot grow the Matrix");

			double[,] data = new double[w, h];
			for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++)
				data[y, x] = _data[y, x];

			return new Matrix(data);
		}

		public Vector ToVector()
		{
			if (Width != 1)
				throw new Exception();

			double[] vec = new double[Height];
			for (int i = 0; i < Height; i++)
				vec[i] = this[i, 0];

			return new Vector(vec);
		}

		#region Operators

		public static Matrix operator +(Matrix a, Matrix b)
		{
			if (a.Width != b.Width || a.Height != b.Height)
				throw new Exception("Matrix sizes do not match");

			double[,] data = new double[a.Height, a.Width];
			for (int y = 0; y < a.Height; y++)
			{
				for (int x = 0; x < a.Width; x++)
				{
					data[y, x] = a[y, x] + b[y, x];
				}
			}

			return new Matrix(data);
		}

		public static Matrix operator -(Matrix a, Matrix b)
		{
			if (a.Width != b.Width || a.Height != b.Height)
				throw new Exception("Matrix sizes do not match");

			double[,] data = new double[a.Height, a.Width];
			for (int y = 0; y < a.Height; y++)
			{
				for (int x = 0; x < a.Width; x++)
				{
					data[y, x] = a[y, x] - b[y, x];
				}
			}

			return new Matrix(data);
		}

		public static Matrix operator *(Matrix a, double b)
		{
			double[,] data = new double[a.Height, a.Width];
			for (int y = 0; y < a.Height; y++)
			{
				for (int x = 0; x < a.Width; x++)
				{
					data[y, x] = a[y, x] * b;
				}
			}

			return new Matrix(data);
		}

		public static Matrix operator *(double b, Matrix a)
		{
			return a * b;
		}

		public static Matrix operator *(Matrix a, Matrix b)
		{
			if (a.Width != b.Height)
				throw new Exception("Matrix sizes are incompatible");

			// The result has the height of the first matrix and the width of the second.
			double[,] data = new double[a.Height, b.Width];
			for (int y = 0; y < a.Height; y++) // Row of the left matrix
			{
				for (int x = 0; x < b.Width; x++) // Column of the right matrix
				{
					double r = 0;
					for (int i = 0; i < a.Width; i++)
					{
						r += a[y, i] * b[i, x];
					}

					data[y, x] = r;
				}
			}

			return new Matrix(data);
		}

		public static Vector operator *(Matrix a, Vector b)
		{
			return (a * b.ToMatrix()).ToVector();
		}

		public static Vector operator *(Vector b, Matrix a)
		{
			return (b.ToMatrix() * a).ToVector();
		}

		#endregion

		#region Generic operations

		public static Matrix Identity(int size = 4)
		{
			double[,] data = new double[size, size];
			for (int i = 0; i < size; i++)
				data[i, i] = 1;
			return new Matrix(data);
		}

		public static Matrix Scale(params double[] s)
		{
			double[,] data = new double[s.Length + 1, s.Length + 1];
			for (int i = 0; i < s.Length; i++)
			{
				data[i, i] = s[i];
			}

			data[s.Length, s.Length] = 1;
			return new Matrix(data);
		}

		public static Matrix Translate(params double[] d)
		{
			Matrix m = Identity(d.Length + 1); // 2 => 3
			for (int i = 0; i < d.Length - 1; i++) // [0,1]
				m._data[i, d.Length - 1] = d[i]; // [0,1], [1,1] = d[0], d[1]
			return m;
		}

		#endregion

		#region 2D operations

		public static Matrix Rotate3(double deg)
		{
			return Rotate3Radian((Math.PI / 180) * deg);
		}

		public static Matrix Rotate3Radian(double rad)
		{
			return Rotate4ZRadian(rad).Truncate(3, 3);
		}

		#endregion

		#region 3D operations

		public static Matrix Rotate4X(double deg)
		{
			return Rotate4XRadian((Math.PI / 180) * deg);
		}

		public static Matrix Rotate4XRadian(double rad)
		{
			return new Matrix(new double[,]
			{
				{1, 0, 0, 0},
				{0, (double) Math.Cos(rad), (double) -Math.Sin(rad), 0},
				{0, (double) Math.Sin(rad), (double) Math.Cos(rad), 0},
				{0, 0, 0, 1},
			});
		}

		public static Matrix Rotate4Y(double deg)
		{
			return Rotate4YRadian((Math.PI / 180) * deg);
		}

		public static Matrix Rotate4YRadian(double rad)
		{
			return new Matrix(new double[,]
			{
				{(double) Math.Cos(rad), 0, (double) Math.Sin(rad), 0},
				{0, 1, 0, 0},
				{(double) -Math.Sin(rad), 0, (double) Math.Cos(rad), 0},
				{0, 0, 0, 1},
			});
		}

		public static Matrix Rotate4Z(double deg)
		{
			return Rotate4ZRadian((Math.PI / 180) * deg);
		}

		public static Matrix Rotate4ZRadian(double rad)
		{
			return new Matrix(new double[,]
			{
				{(double) Math.Cos(rad), (double) -Math.Sin(rad), 0, 0},
				{(double) Math.Sin(rad), (double) Math.Cos(rad), 0, 0},
				{0, 0, 1, 0},
				{0, 0, 0, 1},
			});
		}

		public static Matrix View4(double r, double pitch, double yaw)
		{
			double pitchRad = (Math.PI / 180) * pitch;
			double yawRad = (Math.PI / 180) * yaw;

			double pitchSin = Math.Sin(pitchRad);
			double pitchCos = Math.Cos(pitchRad);
			double yawSin = Math.Sin(yawRad);
			double yawCos = Math.Cos(yawRad);

			return new Matrix(new double[,]
			{
				{-pitchSin, pitchCos, 0, 0},
				{pitchCos * yawCos, yawCos * pitchSin, -yawSin, 0},
				{-pitchCos * yawSin, -pitchSin * yawSin, -yawCos, r},
				{0, 0, 0, 1},
			});
		}

		public static Matrix Projection4(double d, double z)
		{
			return new Matrix(new double[,]
			{
				{d / z, 0, 0, 0},
				{0, d / z, 0, 0},
				{0, 0, 0, 0},
				{0, 0, 0, 1},
			});
		}

		#endregion

		public double this[int y, int x]
		{
			get { return _data[y, x]; }
		}

		public override string ToString()
		{
			var sb = new StringBuilder();

			for (int y = 0; y < Height; y++)
			{
				if (y > 0)
					sb.Append("\n");

				for (int x = 0; x < Width; x++)
				{
					if (x > 0)
						sb.Append(" ");

					sb.Append(string.Format("{0:00}", this[y, x]));
				}
			}

			return sb.ToString();
		}
	}
}