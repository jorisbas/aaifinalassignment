﻿using System;
using System.Collections.Generic;
using System.Drawing;
using AAIFinalAssignment.Behaviors;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment
{
	public class Object2D
	{
		public static Object2DFactory Factory = new Object2DFactory();

		// Position of the object
		public Vector Position { get; set; } = new Vector(0d, 0);

		// Rotation of the object (applied in rendering)
		public Vector Rotation { get; set; } = new Vector(0d);

		// Points of the object
		public List<Vector> Points { get; set; } = new List<Vector>();

		// Edges of the object to draw
		public List<Tuple<int, int>> Edges { get; set; } = new List<Tuple<int, int>>();

		// Current velocity of the object
		public Vector Velocity { get; set; } = new Vector(0d, 0);

		// Max velocity of object in px/s
		public double MaxVelocity { get; set; } = 150;
		public double MaxForce { get; set; } = 25;
		public double Mass { get; set; } = 10;

		public bool DrawDebugInfo { get; set; } = false;

		// List of all the behaviors for this object, with the weight of the behavior
		public List<Tuple<IBehavior, double>> Behaviors { get; set; } = new List<Tuple<IBehavior, double>>();

		public void Update(double deltaTime_s)
		{
			// Initial desired velocity
			Vector desiredVel = new Vector(2);
			double totalWeight = 0;
			foreach (var behavior in Behaviors)
			{
				if (behavior.Item1.Finished) continue;
				desiredVel += behavior.Item1.GetDesiredVelocity(this, deltaTime_s) * behavior.Item2;
				totalWeight += behavior.Item2;
			}

			// Apply weighing
			if (totalWeight > 0)
				desiredVel /= totalWeight;
			else
				desiredVel = new Vector(2);

			// Calculate the steering required
			Vector steering = desiredVel - Velocity;
			steering = steering.Truncate(MaxForce);
			steering /= Mass;

			// Change velocity
			Velocity = (Velocity + steering).Truncate(MaxVelocity);

			// Apply the velocity
			Position += Velocity * deltaTime_s;

			// Rotate based on velocity
			if (Velocity.Length() > 1)
				Rotation[0] = Math.Atan2(Velocity[1], Velocity[0]) - 90 * (Math.PI / 180);

			if (Position[0] < -(Screen.Instance.Width / 2d) ||
			    Position[0] > (Screen.Instance.Width / 2d) ||
			    Position[1] < -(Screen.Instance.Height / 2d) ||
			    Position[1] > (Screen.Instance.Height / 2d))
			{
				Position[0] = 0;
				Position[1] = 1;
			}
		}

		private void Do(Matrix matrix)
		{
			for (var i = 0; i < Points.Count; i++)
			{
				Points.Add(Points[0] * matrix);
				Points.RemoveAt(0);
			}
		}

		public void AddBehavior(IBehavior behavior, double weight)
		{
			Behaviors.Add(new Tuple<IBehavior, double>(behavior, weight));
		}

		public void AddPoint(double x, double y)
		{
			Vector point = new Vector(x, y, 1);
			Points.Add(point);
		}

		public virtual void Draw(Graphics g)
		{
			Vector screenPos = Screen.Instance.ViewportTransform(Position);
			Matrix rotMatrix = CreateRotationMatrix(Rotation[0]);

			// Draw edges
			foreach (Tuple<int, int> edge in Edges)
			{
				Vector point1 = rotMatrix * Points[edge.Item1];
				Vector point2 = rotMatrix * Points[edge.Item2];

				g.DrawLine(Pens.Black,
					(float) (screenPos[0] + point1[0]),
					(float) (screenPos[1] + -point1[1]),
					(float) (screenPos[0] + point2[0]),
					(float) (screenPos[1] + -point2[1]));
			}

			// Draw to every point from the position (center)
			foreach (var point in Points)
			{
				Vector rotatedPoint = rotMatrix * point;
				g.DrawLine(Pens.Red,
					(float) (screenPos[0] + rotatedPoint[0]),
					(float) (screenPos[1] + -rotatedPoint[1]),
					(float) (screenPos[0]),
					(float) (screenPos[1]));
			}

			// Draw debug behavior info
			if (DrawDebugInfo)
			{
				// Textual info
				g.DrawString(ToString(), new Font("Arial", 8), Brushes.LightSlateGray, (float) screenPos[0], (float) screenPos[1]);

				// Velocity vector
				var ppv = Screen.Instance.ViewportTransform(Position + Velocity);
				g.DrawLine(Pens.Blue, (float) screenPos[0], (float) screenPos[1], (float) ppv[0], (float) ppv[1]);

				// Behavior debug info
				foreach (var behavior in Behaviors)
				{
					behavior.Item1.Draw(g, this);
				}
			}
		}

		public static Matrix CreateRotationMatrix(double alpha)
		{
			return Matrix.Rotate3Radian(alpha);
		}

		public override string ToString()
		{
			return
				$"Pos:[{Position[0]:0},{Position[1]:0}]{Environment.NewLine}" +
				$"Rot:{Rotation[0]:0.##} Deg:{Rotation[0] * (180 / Math.PI) + 270:0.#}{Environment.NewLine}" +
				$"MaxV:{MaxVelocity} V:{Velocity.Length():0.##}{Environment.NewLine}" +
				$"MaxF:{MaxForce} Mass:{Mass}{Environment.NewLine}";
		}
	}
}