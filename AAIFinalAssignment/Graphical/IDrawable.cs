﻿using AAIFinalAssignment.Compute;
using System.Drawing;

namespace AAIFinalAssignment.Graphical
{
    public interface IDrawable
    {
        void Draw(Graphics g, Matrix vp);
    }
}
