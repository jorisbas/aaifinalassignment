﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Graphical
{
    public abstract class Shape : IDrawable
    {

        public abstract IEnumerable<Vector> Points { get; }
        public abstract IEnumerable<Tuple<int, int>> Edges { get; }

        private Pen _pen = new Pen(Color.Black);

        public virtual void Draw(Graphics g, Matrix vp)
        {
            foreach (Tuple<int,int> edge in Edges)
            {
                Vector a = vp * Points.ElementAt(edge.Item1);
                Vector b = vp * Points.ElementAt(edge.Item2);
                g.DrawLine(_pen, (float)a.X, (float)a.Y, (float)b.X, (float)b.Y);
            }
        }

    }
}
