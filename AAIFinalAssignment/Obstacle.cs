﻿using System.Drawing;

namespace AAIFinalAssignment
{
	public class Obstacle : Object2D
	{
		public double Size { get; set; } = 50;

		public override void Draw(Graphics g)
		{
			base.Draw(g);
			var posTrans = Screen.Instance.ViewportTransform(Position);
			g.DrawEllipse(Pens.Black, 
				(float) (posTrans[0] - Size / 2),
				(float) (posTrans[1] - Size / 2), 
				(float) Size,
				(float) Size);
		}
	}
}