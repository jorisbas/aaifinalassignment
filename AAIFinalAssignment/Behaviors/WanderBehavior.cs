﻿using System;
using System.Drawing;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Behaviors
{
	public class WanderBehavior : IBehavior
	{
		public double CircleDist;
		public double CircleRadius;
		public double MaxAngleChange;
		public Random Random;

		// TODO should this be object specific.
		public double WanderAngle;

		public WanderBehavior(double circleDist, double circleRadius, double maxAngleChange = 0.7d, Random random = null)
		{
			CircleDist = circleDist;
			CircleRadius = circleRadius;
			MaxAngleChange = maxAngleChange;
			Random = random ?? new Random();
		}

		public bool Finished { get; set; }

		public Vector GetDesiredVelocity(Object2D obj, double deltaTime_s)
		{
			WanderAngle += Random.NextDouble() * MaxAngleChange - MaxAngleChange * 0.5;

			Vector circleCenter = obj.Velocity.Normalize() * CircleDist;
			Vector displacement = new Vector(0d, CircleRadius);
            Matrix rotMatrix = Matrix.Rotate3Radian(WanderAngle).Truncate(2, 2);
//			Matrix rotMatrix = new Matrix(new[,]
//			{
//				{Math.Cos(WanderAngle), -Math.Sin(WanderAngle)}, {Math.Sin(WanderAngle), Math.Cos(WanderAngle)}
//			});
			displacement = rotMatrix * displacement;
			return circleCenter + displacement;
		}

		public void Draw(Graphics g, Object2D obj)
		{
			Vector circleCenter = obj.Velocity.Normalize() * CircleDist;
			circleCenter = Screen.Instance.ViewportTransform(obj.Position + circleCenter);

			// Draw slow radius
			g.DrawEllipse(Pens.Green,
				(float) (circleCenter[0] - CircleRadius),
				(float) (circleCenter[1] - CircleRadius),
				(float) (CircleRadius * 2),
				(float) (CircleRadius * 2));

			// Draw angle
			Vector displacement = new Vector(0d, CircleRadius);
			double rot = WanderAngle + 180 * (Math.PI / 180);

            Matrix rotMatrix = Matrix.Rotate3Radian(rot).Truncate(2, 2);

			displacement = rotMatrix * displacement;
			displacement[0] = -displacement[0];
			displacement = circleCenter + displacement;
			g.DrawLine(Pens.LawnGreen,
				(float) (circleCenter[0]),
				(float) (circleCenter[1]),
				(float) (displacement[0]),
				(float) (displacement[1]));
		}
	}
}