﻿using System;
using System.Drawing;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Behaviors
{
	public class ObstacleAvoidanceBehavior : IBehavior
	{
		public bool Finished { get; set; }

		public Vector GetDesiredVelocity(Object2D obj, double deltaTime_s)
		{
			Finished = true;
			Object2D rect = new Object2D();
			rect.AddPoint(-10, 0);
			rect.AddPoint(10, 0);
			rect.AddPoint(10, 100);
			rect.AddPoint(-10, 100);


			return obj.Velocity;
		}

		public void Draw(Graphics g, Object2D obj)
		{
			Object2D rect = new Object2D();
			rect.AddPoint(-10, 0);
			rect.AddPoint(10, 0);
			rect.AddPoint(10, 100);
			rect.AddPoint(-10, 100);
			rect.Edges.Add(new Tuple<int, int>(0, 1));
			rect.Edges.Add(new Tuple<int, int>(1, 2));
			rect.Edges.Add(new Tuple<int, int>(2, 3));
			rect.Edges.Add(new Tuple<int, int>(3, 0));

			rect.Position = obj.Position;
			rect.Rotation = obj.Rotation;
			
			rect.Draw(g);
		}
	}
}