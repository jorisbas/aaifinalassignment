﻿using System;
using System.Drawing;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Behaviors
{
	public class SeekBehavior : IBehavior
	{
		public Vector Target;

		public SeekBehavior(Vector target)
		{
			Target = target ?? throw new ArgumentNullException(nameof(target));
		}

		public bool Finished { get; set; }

		public virtual Vector GetDesiredVelocity(Object2D obj, double deltaTime_s)
		{
			return (Target - obj.Position).Normalize() * obj.MaxVelocity;
		}

		public void Draw(Graphics g, Object2D obj)
		{
			// Draw seek target
			var target = Screen.Instance.ViewportTransform(Target);
			g.DrawLine(Pens.Black
				, (float) target[0]
				, (float) target[1] + 5
				, (float) target[0]
				, (float) target[1] - 5);
			g.DrawLine(Pens.Black
				, (float) target[0] + 5
				, (float) target[1]
				, (float) target[0] - 5
				, (float) target[1]);
		}
	}
}