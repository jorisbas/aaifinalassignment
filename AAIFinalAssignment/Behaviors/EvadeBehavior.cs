﻿using System.Drawing;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Behaviors
{
	public class EvadeBehavior : IBehavior
	{
		private PursueBehavior _pursueBehavior;

		public EvadeBehavior(PursueBehavior pursueBehavior)
		{
			_pursueBehavior = pursueBehavior;
		}

		public bool Finished { get; set; }

		public Vector GetDesiredVelocity(Object2D obj, double deltaTime_s)
		{
			return -_pursueBehavior.GetDesiredVelocity(obj, deltaTime_s);
		}

		public void Draw(Graphics g, Object2D obj)
		{
			_pursueBehavior.Draw(g, obj);
		}
	}
}