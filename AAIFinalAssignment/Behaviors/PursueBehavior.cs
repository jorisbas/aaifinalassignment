﻿using System.Drawing;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Behaviors
{
	public class PursueBehavior : IBehavior
	{
		private ArrivalBehavior _arrivalBehavior;

		public PursueBehavior(ArrivalBehavior arrivalBehavior)
		{
			_arrivalBehavior = arrivalBehavior;
		}

		public bool Finished { get; set; }

		public Vector GetDesiredVelocity(Object2D obj, double deltaTime_s)
		{
			_arrivalBehavior.TargetOffset = new Vector(0, _arrivalBehavior.TargetObject.Velocity.Length() * (_arrivalBehavior.TargetObject.MaxVelocity / obj.MaxVelocity));

			return _arrivalBehavior.GetDesiredVelocity(obj, deltaTime_s);
		}

		public void Draw(Graphics g, Object2D obj)
		{
			_arrivalBehavior.Draw(g, obj);
		}
	}
}