﻿using System;
using System.Drawing;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Behaviors
{
	public class ArrivalBehavior : IBehavior
	{
		public Object2D TargetObject;
		public Vector TargetOffset;

		public double SlowingRadius;

		public double AchieveRadius;

		public ArrivalBehavior(Vector target, double slowingRadius, double achieveRadius = 0d)
		{
			TargetObject = new Object2D();
			TargetObject.Position = target ?? throw new ArgumentNullException(nameof(target));
			TargetOffset = new Vector(2);

			SlowingRadius = slowingRadius;
			AchieveRadius = achieveRadius;
		}

		public ArrivalBehavior(Object2D target, Vector offset, double slowingRadius)
		{
			TargetObject = target;
			TargetOffset = offset;
			SlowingRadius = slowingRadius;
		}

		public bool Finished { get; set; }

		public Vector GetDesiredVelocity(Object2D obj, double deltaTime_s)
		{
			Vector desiredVel = GetTargetVector() - obj.Position;
			double distToTarget = desiredVel.Length();
			if (distToTarget < SlowingRadius)
			{
				if (AchieveRadius > 0 && distToTarget < AchieveRadius)
				{
					Finished = true;
				}

				desiredVel = desiredVel.Normalize() * obj.MaxVelocity * (distToTarget / SlowingRadius);
			}
			else
				desiredVel = desiredVel.Normalize() * obj.MaxVelocity;

			return desiredVel;
		}

		public void Draw(Graphics g, Object2D obj)
		{
			// Draw seek target
			var target = Screen.Instance.ViewportTransform(GetTargetVector());
			g.DrawLine(Pens.Black
				, (float) target[0]
				, (float) target[1] + 5
				, (float) target[0]
				, (float) target[1] - 5);
			g.DrawLine(Pens.Black
				, (float) target[0] + 5
				, (float) target[1]
				, (float) target[0] - 5
				, (float) target[1]);

			// Draw slow radius
			g.DrawEllipse(Pens.Green,
				(float) (target[0] - SlowingRadius),
				(float) (target[1] - SlowingRadius),
				(float) (SlowingRadius * 2),
				(float) (SlowingRadius * 2));
		}

		private Vector GetTargetVector()
		{
			var targetVector = TargetObject.Position;
			var a = TargetObject.Rotation[0];
			var offset = TargetOffset;
			var offsetRot = new Vector(Math.Cos(a) * offset[0] - Math.Sin(a) * offset[1],
				Math.Sin(a) * offset[0] + Math.Cos(a) * offset[1]);
			targetVector += offsetRot;
			return targetVector;
		}
	}
}