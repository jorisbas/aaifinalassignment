﻿using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Behaviors
{
	public class FleeBehavior : SeekBehavior
	{
		public FleeBehavior(Vector target) : base(target)
		{
		}

		public override Vector GetDesiredVelocity(Object2D obj, double deltaTime_s)
		{
			return -base.GetDesiredVelocity(obj, deltaTime_s);
		}
	}
}