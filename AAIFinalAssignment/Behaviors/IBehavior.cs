﻿using System.Drawing;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Behaviors
{
	public interface IBehavior
	{
		bool Finished { get; set; }
		
		Vector GetDesiredVelocity(Object2D obj, double deltaTime_s);

		void Draw(Graphics g, Object2D obj);
	}
}