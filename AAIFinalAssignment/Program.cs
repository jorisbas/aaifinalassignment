﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using AAIFinalAssignment.Behaviors;
using Timer = System.Timers.Timer;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment
{
	public class Program
	{
		public static List<Object2D> Objects = new List<Object2D>();
		public static List<Obstacle> Obstacles = new List<Obstacle>();

		public static readonly string InstructionsString = $"Instructions:{Environment.NewLine}" +
		                                                   $"F3: Toggle debug info{Environment.NewLine}" +
		                                                   $"MouseLeft: Change arrival point{Environment.NewLine}" +
		                                                   $"MouseRight: Set pursuer pos{Environment.NewLine}";

		public static void Main(string[] args)
		{
            Screen screen = new Screen();
			// TODO fix screens
            // screen.Show();
            // Application.Run(screen);

            // Form setup
			int formWidth = 800;
			int formHeight = 600;
			Screen.Instance.Width = formWidth;
			Screen.Instance.Height = formHeight;
			Form form = new Form();
			form.ClientSize = new Size(formWidth, formHeight);

			// Object setup
			Object2D o2D = Object2D.Factory.CreateTriangle(100);
			o2D.Position = new Vector(new[] {100d, 100});
			SeekBehavior seekBehavior = new SeekBehavior(new Vector(new[] {200d, 100}));
			ArrivalBehavior arrivalBehavior =
				new ArrivalBehavior(new Vector(2), (o2D.MaxVelocity / 10) * (o2D.Mass / 2) / 1.25, 5);
			o2D.AddBehavior(arrivalBehavior, 1111);
			WanderBehavior wanderBehavior = new WanderBehavior(100, 20);
			o2D.AddBehavior(wanderBehavior, 1);
			ObstacleAvoidanceBehavior obstacleAvoidanceBehavior = new ObstacleAvoidanceBehavior();
			// o2D.AddBehavior(obstacleAvoidanceBehavior, 1);
			o2D.DrawDebugInfo = true;
			Objects.Add(o2D);


			Object2D pursuer = Object2D.Factory.CreateTriangle(50);
			pursuer.Position = new Vector(new[] {200d, 0});
			var offVecPers = new Vector(new[] {0d, 0});
			pursuer.AddBehavior(new PursueBehavior(new ArrivalBehavior(o2D, offVecPers, 50)), 1);
			pursuer.DrawDebugInfo = true;
			pursuer.Mass = 1;
			pursuer.MaxForce = 150;
			pursuer.MaxVelocity = 100;
			Objects.Add(pursuer);

			Obstacles.Add(Object2D.Factory.CreateObstacle(0, 0, 100));

			// Update timer
			Timer timer = new Timer(10);
			timer.Elapsed += (sender, eventArgs) =>
			{
				double dt = 10 / 1000d;
				Objects.ForEach(o => o.Update(dt));
				form.Invalidate();
			};
			timer.AutoReset = true;
			timer.Enabled = true;

			// Paint
			form.Paint += (sender, eventArgs) =>
			{
				Graphics g = eventArgs.Graphics;
				Objects.ForEach(o => o.Draw(g));
				Obstacles.ForEach(o => o.Draw(g));
				g.DrawString(InstructionsString, new Font("Arial", 10), Brushes.LightSlateGray, 5f, 5f);
            };

			// Keys
			form.KeyDown += (sender, eventArgs) =>
			{
				switch (eventArgs.KeyCode)
				{
					case Keys.Escape:
						Environment.Exit(0);
						break;
					case Keys.F3:
						Objects.ForEach(o => o.DrawDebugInfo = !o.DrawDebugInfo);
						break;
				}
			};

			// Clicks
			form.Click += (sender, eventArgs) =>
			{
				if (!(eventArgs is MouseEventArgs e)) return;
				var clickPos = new Vector(new double[] {e.X, e.Y});
				var transformed = Screen.ViewportTransform(clickPos, -formWidth, formHeight);
				switch (e.Button)
				{
					case MouseButtons.Left:
						arrivalBehavior.TargetObject.Position = transformed;
						arrivalBehavior.Finished = false;
						break;
					case MouseButtons.Right:
						pursuer.Position = transformed;
						break;
					case MouseButtons.None:
						break;
					case MouseButtons.Middle:
						break;
					case MouseButtons.XButton1:
						break;
					case MouseButtons.XButton2:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			};

			VectorExtensions.EnableDoubleBuffer(form);

			// Start app
			Application.Run(form);
		}

		private static void AddChildPlanes(int i, Object2D parent)
		{
			for (int j = 0; j < 2; j++)
			{
				Object2D o = Object2D.Factory.CreateTriangle(50);
				var offVec = new Vector(new[] {40d * (j == 0 ? i : -i), -35 * i});
				o.AddBehavior(new ArrivalBehavior(parent, offVec, 50), 1);
				o.DrawDebugInfo = true;
				o.Mass = 1;
				o.MaxForce = 50;
				o.MaxVelocity = 300;
				Objects.Add(o);

				if (i > 1)
				{
					AddChildPlanes(i - 1, o);
				}
			}
		}
	}
}