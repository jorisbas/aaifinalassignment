﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AAIFinalAssignment.Extensions
{
    public class CopyList<TElement> : IList<TElement>
    {

        private TElement[] _elements;
        private object _lock;

        public CopyList() : this(new TElement[0])
        {
        }

        public CopyList(IEnumerable<TElement> elems)
        {
            _elements = elems.ToArray();
            _lock = new object();
        }

        public TElement this[int index] {
            get
            {
                return _elements[index];
            }
            set
            {
                _elements[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return _elements.Length;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public void Add(TElement item)
        {
            lock (_lock)
            {
                var elems = _elements;
                var arr = new TElement[elems.Length + 1];
                elems.CopyTo(arr, 0);
                arr[arr.Length - 1] = item;
                _elements = arr;
            }
        }

        public void Clear()
        {
            lock (_lock)
            {
                _elements = new TElement[0];
            }
        }

        public bool Contains(TElement item)
        {
            return _elements.Contains(item);
        }

        public void CopyTo(TElement[] array, int arrayIndex)
        {
            _elements.CopyTo(array, arrayIndex);
        }

        public IEnumerator<TElement> GetEnumerator()
        {
            return ((IEnumerable<TElement>)_elements).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _elements.GetEnumerator();
        }

        public int IndexOf(TElement item)
        {
            var elems = _elements;
            for (int i = 0; i < elems.Length; i++)
                if (elems[i].Equals(item))
                    return i;
            return -1;
        }

        public void Insert(int index, TElement item)
        {
            lock (_lock)
            {
                var elems = _elements;
                var arr = new TElement[elems.Length + 1];

                // before index
                for (int i = 0; i < index; i++)
                    arr[i] = elems[i];

                // index
                arr[index] = item;

                // after index
                for (int i = index + 1; i < arr.Length; i++)
                    arr[i] = elems[i - 1];

                _elements = arr;
            }
        }

        public bool Remove(TElement item)
        {
            lock (_lock)
            {
                int index = IndexOf(item);
                if (index == -1)
                    return false;
                RemoveAt(index);
                return true;
            }
        }

        public void RemoveAt(int index)
        {
            lock (_lock)
            {
                var elems = _elements;
                var arr = new TElement[elems.Length - 1];
                for (int i = 0; i < index; i++)
                    arr[i] = elems[i];
                for (int i = index; i < arr.Length; i++)
                    arr[i] = elems[i + 1];
                _elements = arr;
            }
        }

        public void RemoveAll(Func<TElement, bool> pred)
        {
            lock (_lock)
            {
                _elements = _elements
                    .Where(e => !pred(e))
                    .ToArray();
            }
        }

        public void Transact(Action act)
        {
            lock (_lock)
            {
                act();
            }
        }

        public T Transact<T>(Func<T> act)
        {
            lock (_lock)
            {
                return act();
            }
        }

    }
}
