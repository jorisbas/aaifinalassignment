﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AAIFinalAssignment.Extensions
{
    public static class LinqExtensions
    {

        public static string Join<TElement>(this IEnumerable<TElement> self, string sep = ", ")
        {
            return self.Aggregate("", (acc, cur) => (acc.Length > 0 ? (acc + sep) : "") + cur);
        }

        public static void ForEach<TElement>(this IEnumerable<TElement> self, Action<TElement> func)
        {
            foreach (TElement elem in self)
                func(elem);
        }

        public static CopyList<TElement> ToCopyList<TElement>(this IEnumerable<TElement> self)
        {
            return new CopyList<TElement>(self);
        }

    }
}
