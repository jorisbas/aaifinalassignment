﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAIFinalAssignment.Compute;

namespace AAIFinalAssignment.Universe
{
    public class PhysicsEntity : Entity
    {

        public Vector Velocity { get; set; }
        public double MaxVelocity { get; set; }
        public double MaxForce { get; set; }
        public double Mass { get; set; }

        public PhysicsEntity(Vector position = null, Vector velocity = null, double maxVelocity = 0, double maxForce = 0, double mass = 0) : base(position)
        {
            Velocity = velocity;
            MaxVelocity = maxVelocity;
            MaxForce = maxForce;
            Mass = mass;
        }

        public override void Update(double dt)
        {
            // TODO: Bas mag dit oplossen
        }

    }
}
