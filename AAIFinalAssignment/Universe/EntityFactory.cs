﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAIFinalAssignment.Universe
{
    public static class EntityFactory
    {

        public static Entity CreateEntity() => new Entity();
        public static Entity CreatePhysicsEntity() => new PhysicsEntity();

    }
}
