﻿using AAIFinalAssignment.Compute;
using AAIFinalAssignment.Graphical;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AAIFinalAssignment.Universe
{
    public class Entity : IDrawable, BoundingBox.IIntersector, BoundingBox.IContainable
    {

        public Vector Position { get; set; }
        public double Rotation { get; set; }

        private List<Vector> _points { get; }
        private IEnumerable<Vector> _worldPoints { get => _points.Select(p => Position + p); }
        private List<Tuple<int, int>> _edges { get; }

        public Entity(Vector position = null)
        {
            Position = position ?? new Vector(0d, 0d);
            _points = new List<Vector>();
            _edges = new List<Tuple<int, int>>();
        }

        public virtual void Update(double dt) { }
        public virtual void Draw(Graphics g, Matrix vp)
        {
            Matrix matTrans = Matrix.Translate(Position[0], Position[1]);
            Matrix matRot = Matrix.Rotate3Radian(Rotation);

            List<Vector> points = _points
              .Select(p => vp * matTrans * matRot * p.Resize(3))
              .ToList();

            foreach (Tuple<int, int> edge in _edges)
            {
                g.DrawLine(null, (PointF)points[edge.Item1], (PointF)points[edge.Item2]);
            }
        }

        public void AddPoint(double x, double y) => AddPoint(new Vector(x, y));
        public void AddPoint(Vector v) => _points.Add(v);
        public void AddEdge(int a, int b) => _edges.Add(new Tuple<int, int>(a, b));

        public virtual BoundingBox ToBoundingBox()
        {
            if (_points.Count < 1)
                return null;

            double minX = Double.MaxValue;
            double minY = Double.MaxValue;
            double maxX = Double.MinValue;
            double maxY = Double.MinValue;
            
            foreach (Vector point in _worldPoints)
            {
                minX = Math.Min(minX, point[0]);
                minY = Math.Min(minY, point[1]);
                maxX = Math.Max(maxX, point[0]);
                maxY = Math.Max(maxY, point[1]);
            }

            return new BoundingBox(minX, minY, maxX, maxY);
        }

        public bool Intersects(BoundingBox bb)
        {
            return _worldPoints.Any(v => v.ContainedIn(bb));
        }

        public bool ContainedIn(BoundingBox bb)
        {
            return _worldPoints.All(v => v.ContainedIn(bb));
        }

    }
}
