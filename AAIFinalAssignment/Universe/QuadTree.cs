﻿﻿using AAIFinalAssignment.Compute;
using AAIFinalAssignment.Extensions;
using AAIFinalAssignment.Graphical;
 using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AAIFinalAssignment.Universe
{
    public class QuadTree<T> : IEnumerable<T>, IDrawable
        where T : BoundingBox.IIntersector, BoundingBox.IContainable
    {

        public QuadTree<T> Parent { get; }
        public BoundingBox Rect { get; }

        private CopyList<QuadTree<T>> _children;
        public bool HasChildren { get { return _children.Count > 0; } }

        private CopyList<T> _elements;

        // Amount of elements contained in this tree.
        // When (_contents.Length > _maxCount), the tree splits.
        private int _maxCount;

        private int _depth;
        private int _maxDepth;

        public QuadTree(BoundingBox rect, int maxDepth = 5, int maxCount = 10, int depth = 0)
            : this(null, rect, maxDepth, maxCount, depth)
        {
        }

        public QuadTree(QuadTree<T> parent, BoundingBox rect, int maxDepth = 5, int maxCount = 10, int depth = 0)
        {
            Parent = parent;
            Rect = rect;
            _children = new CopyList<QuadTree<T>>();
            _elements = new CopyList<T>();
            _maxCount = maxCount;
            _depth = depth;
            _maxDepth = maxDepth;

            _brush = new SolidBrush(Color.Red);
            _pen = new Pen(_brush, _depth);
            _font = new Font("Arial", 8);
        }

        public IEnumerable<T> Intersect(BoundingBox rect)
        {
            return _elements
                .Where(elem => elem.Intersects(rect))
                .Concat(_children.SelectMany(c => c.Intersect(rect)));
        }

        public IEnumerable<T> Candidates(Vector vec)
        {
            if (!vec.ContainedIn(Rect))
                return new T[0];

            return _elements.Concat(_children.SelectMany(c => c.Candidates(vec)));
        }

        #region IDrawable methods

        private Brush _brush;
        private Pen _pen;
        private Font _font;

        public void Draw(Graphics g, Matrix vp)
        {
            // Draw this tree
            g.DrawRectangle(_pen, (float)Rect.Left, (float)Rect.Top, (float)Rect.Width, (float)Rect.Height);

            int count = 0;
            IEnumerable<T> elems = null; 

            _elements.Transact(() =>
           {
               count = _elements.Count;
               elems = _elements.ToArray();
            });

            // If there are no children, draw the amount of elements
            if (!HasChildren)
                g.DrawString($"{ count }", _font, _brush, (float)Rect.Left + 5f, (float)Rect.Top + 5f);

            var drawables = _children.Cast<IDrawable>();

            // Draw the entities if applicable
            if (typeof(IDrawable).IsAssignableFrom(typeof(T)))
                drawables = drawables.Concat(elems.Cast<IDrawable>());

            drawables.ForEach(d => d.Draw(g, vp));
        }

        #endregion

        #region ICollection<T> methods

        public int Count
        {
            get
            {
                return _elements.Count + _children.Sum(child => child.Count);
            }
        }

        public void Add(T item)
        {
            int i = _getIndex(item);
            if (i != -1)
            {
                _children[i].Add(item);
                return;
            }

            _elements.Add(item);
            _split();
        }

        public void Clear()
        {
            _children = new CopyList<QuadTree<T>>();
            _elements.Clear();
        }

        public bool Contains(T item)
        {
            return _elements.Contains(item) || _children.Any(c => c.Contains(item));
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _elements
                .Concat(_children.SelectMany(c => c))
                .GetEnumerator();
        }

        public bool Remove(T item)
        {
            if (_elements.Remove(item) || _children.Any(c => c.Remove(item)))
            {
                _merge();
                return true;
            }
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Helper methods

        private void _split()
        {
            _elements.Transact(() =>
            {
                _children.Transact(() =>
                {
                    if (HasChildren || _elements.Count <= _maxCount || _depth >= _maxDepth)
                        return;

                    _children.Add(new QuadTree<T>(Rect.TopLeftQuadrant, _maxDepth, _maxCount, _depth + 1));
                    _children.Add(new QuadTree<T>(Rect.TopRightQuadrant, _maxDepth, _maxCount, _depth + 1));
                    _children.Add(new QuadTree<T>(Rect.BottomLeftQuadrant, _maxDepth, _maxCount, _depth + 1));
                    _children.Add(new QuadTree<T>(Rect.BottomRightQuadrant, _maxDepth, _maxCount, _depth + 1));

                    _elements.RemoveAll(elem =>
                    {
                        int i = _getIndex(elem);
                        if (i == -1)
                            return false;

                        _children[i].Add(elem);
                        return true;
                    });
                });
            });
        }

        private void _merge()
        {
            _elements.Transact(() =>
            {
                _children.Transact(() =>
                {
                    if (!HasChildren || Count > _maxCount)
                        return;

                    _children.ForEach(child => child.ForEach(elem => _elements.Add(elem)));
                    _children.Clear();
                });
            });
        }

        private int _getIndex(T elem)
        {
            return _children.Transact(() =>
            {
                if (!HasChildren)
                    return -1;

                for (int i = 0; i < _children.Count; i++)
                {
                    var tree = _children[i];
                    if (elem.ContainedIn(tree.Rect))
                        return i;
                }

                return -1;
            });
        }

        #endregion

    }
}
