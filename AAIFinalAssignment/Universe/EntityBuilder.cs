﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAIFinalAssignment.Universe
{
    public class EntityBuilder
    {

        private Func<Entity> _factory;
        public EntityBuilder(Func<Entity> factory)
        {
            _factory = factory;
        }

        public Entity Square(double size)
        {
            return Rectangle(size, size);
        }

        public Entity Rectangle(double width, double height)
        {
            width /= 2;
            height /= 2;

            Entity e = _factory();
            e.AddPoint(-width, -height);
            e.AddPoint(width, -height);
            e.AddPoint(width, height);
            e.AddPoint(-width, height);
            e.AddEdge(0, 1);
            e.AddEdge(1, 2);
            e.AddEdge(2, 3);
            e.AddEdge(3, 0);
            return e;
        }

    }
}
