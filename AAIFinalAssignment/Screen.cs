﻿using AAIFinalAssignment.Compute;
using AAIFinalAssignment.Universe;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace AAIFinalAssignment
{
	public partial class Screen : Form
	{

        private QuadTree<Entity> _entities;

        public Screen()
        {
            _entities = new QuadTree<Entity>(new BoundingBox(Width, Height));
        }



		private static Screen _instance;
		public static Screen Instance => _instance ?? (_instance = new Screen());

		public Vector ViewportTransform(Vector vector)
		{
			return ViewportTransform(vector, Width, Height);
		}

		public static Vector ViewportTransform(Vector vector, int width, int height)
		{
			return new Vector(vector[0] + width / 2d, height / 2d - vector[1]);
		}

	}
}